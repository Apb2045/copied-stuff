#pragma once

struct listNode {
    int data;
    listNode* next;
    listNode(int x=0,listNode* n=0):data(x),next(n) {}
};

class list {
public:
    list(); /* constructor */
    list(const list& L);
    ~list();
    list& operator=(const list& RHS);
    void insertAtFront(int x);
    void clear();
    bool eraseOne(int x);
private:
    listNode* root;
};
