include "list.h"

list::list() {
    /* set list to be empty: */
    root = 0;
}

list::list(const list& L) {
    /* TODO: copy L into *this.
     * NOTE: this is kind of tricky...  try the destructor first. */
}

list::~list() {
    /* TODO: free all the nodes. */
    this->clear();
    
}

//is the destructor the same as

/*
 void list::clear() {
	listNode* doomed = this->root;
	while(this->root) {
 this->root = this->root->next;
 delete doomed;
 doomed = this->root;
	}
 }
 
 */


/* TODO: make sure the functions we wrote in the notes
 * actually work out.  (Write + test them here.) */

void list::clear()
{
    listnode * doomed = this->root;
    
    while(doomed)
    {
        this->root = doomed->next;
        delete doomed;
        doomed = this->root;
    }
}

void list::insertAtFront(int x)
{
    listnode * newNode = new listnode;
    newNode->data = x;
    newNode->next = this->root;
    this->root = newNode;
}

bool list::eraseOne(int x)
{
    //listnode * target = new listnode; //dont need this because im not dynamically allocating memory
    //listnode * guyBefore = new listnode; //dont need this because i can just create the pointer in the
    //target->next = this->root; can do this more simply
    //guyBefore->next = 0;
    listnode * guyBefore = 0;
    listnode * target = this->root;
    
    
    while(target != 0 && target->data != x) //could write target != 0 simply as target
    {
        guyBefore = target;
        target = target->next;
    }
    
    if(!target) return false;
    /*
     if(guyBefore)
     {
     guyBefore->next = target->next;
     delete target;
     }
     
     else
     {
     this->root = target->next;
     delete target;
     }
     */
    //simplify 
    
    if(!guyBefore) {this->root = target->next};
    
    else {guyBefore->next = target->next};
    
    delete target;
    
    return true;
}
