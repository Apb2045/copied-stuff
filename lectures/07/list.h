#pragma once

struct listNode {
	int data;
	listNode* next;
	listNode(int x=0,listNode* n=0):data(x),next(n) {}
	/* NOTE: the : data(x)... is an 'initializer list'.
	 * You can directly invoke constructors for member
	 * variables this way. (this could save you from
	 * initializing something with a default constructor
	 * and then immediately overwriting with assignment.) */
};

class list {
public:
	list(); /* constructor */
	list(const list& L);
	~list();
	list& operator=(const list& RHS);
	void insertAtFront(int x);
	bool eraseOne(int x);
	void print();
	void clear();
	void reverse(); /* reverses order of list. */
private:
	listNode* root;
};
