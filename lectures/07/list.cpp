#include "list.h"
#include <iostream>
using std::cout;
using std::endl;
	
list::list() {
	/* set list to be empty: */
	root = 0;
}
/*
	My Thoughts
	the copy's root points to null
	we create a pointer pL which is pointing to the same place as the root of the original
	we create a pointer p  which points to the address of the originals root
	while pL is not pointing to null 
		the address of 
	
*/
list::list(const list& L) { //copy constructor for linked list
	this->root = 0;//root of new  list points to null
	listNode* pL = L.root;//create a new listNode which points to the same place as the root of the list being copied
	listNode** p = &this->root;//see written notes
	while (pL != 0) {//complete this loop until the root of the list being copied is null
		*p = new listNode(pL->data);//	
		pL = pL->next;
		p = &(*p)->next;
	}
	/* TODO: try to write this without the listNode** stuff.
	 * (You will probably need a special case for the first node...) */
}

list& list::operator=(const list& RHS) {
	/* remember: assignment is just like the copy constructor, but you have to:
	 * 1. check for self-assignment
	 * 2. clean up existing object before copying. */
	if (this == &RHS) return *this;
	this->clear();
	/* now just use the copy constructor code... */
	return *this;
}
/* TODO: write another version of the copy constructor that does not
 * blindly deallocate the LHS nodes, but rather re-uses them, and
 * either (a) deletes the extras if present, or (b) allocates new
 * ones if needed. */

list::~list() {
	this->clear();
}

void list::insertAtFront(int x) {
	this->root = new listNode(x,this->root);
}

bool list::eraseOne(int x) {
	listNode* target = this->root;
	listNode* guyBefore = 0;
	/* now do the search for x: */
	while (target && target->data != x) {
		guyBefore = target;
		target = target->next; /* highly analogous to i++ */
	}
	if (!target) return false; /* x was not found */
	/* unfortunately we need a special case for removing
	 * the first node: */
	if (!guyBefore)
		this->root = target->next;
	else
		guyBefore->next = target->next;
	delete target;
	return true;
}

void list::clear() {
	listNode* doomed = this->root;
	while(this->root) {
		this->root = this->root->next;
		delete doomed;
		doomed = this->root;
	}
}
void list::print() {
	for (listNode* p = this->root; p !=0; p = p->next) {
		cout << p->data << " ";
	}
	cout << endl;
}

void list::reverse() {
	/* NOTE: we should do this WITHOUT allocating any memory.
	 * Just rearrange the arrows until the list is reversed.
	 * Put another way, memory footprint should be O(1). */
	/* handle special case of empty list: */
	if (this->root == 0) return;
	listNode* q = 0; /* node before p */
	listNode* p = root; /* node we're currently reversing */
	listNode* r = p->next; /* node after p */
	/* now loop through list and reverse p over and over. */
	while (r) {
		/* reverse node at p */
		p->next = q; /* p is now reversed */
		/* now fix variable meanings: */
		q = p;
		p = r;
		r = r->next;
	}
	/* when this ends, the last node (pointed to by p) will still
	 * need to be reversed, and the root should be set to p. */
	root = p;
	p->next = q;
	/* TODO: delete this and do it from scratch.  */
}
