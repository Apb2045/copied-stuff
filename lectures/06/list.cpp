#include "list.h"
#include <iostream>
using std::cout;

list::list() {
	/* set list to be empty: */
	root = 0;
}

list::list(const list& L) {
	this->root = 0;
	listNode* pL = L.root;
	listNode** p = &this->root;
	while (pL != 0) {
		*p = new listNode(pL->data);
		pL = pL->next;
		p = &(*p)->next;
	}
	/* TODO: try to write this without the listNode** stuff.
	 * (You will probably need a special case for the first node...) */
}

list& list::operator=(const list& RHS) {
	/* remember: assignment is just like the copy constructor, but you have to:
	 * 1. check for self-assignment
	 * 2. clean up existing object before copying. */
	if (this == &RHS) return *this;
	this->clear();
	/* now just use the copy constructor code... */
	return *this;
}
/* TODO: write another version of the copy constructor that does not
 * blindly deallocate the LHS nodes, but rather re-uses them, and
 * either (a) deletes the extras if present, or (b) allocates new
 * ones if needed. */

list::~list() {
	this->clear();
}

void list::insertAtFront(int x) {
	this->root = new listNode(x,this->root);
}

bool list::eraseOne(int x) {
	listNode* target = this->root;
	listNode* guyBefore = 0;
	/* now do the search for x: */
	while (target && target->data != x) {
		guyBefore = target;
		target = target->next; /* highly analogous to i++ */
	}
	if (!target) return false; /* x was not found */
	/* unfortunately we need a special case for removing
	 * the first node: */
	if (!guyBefore)
		this->root = target->next;
	else
		guyBefore->next = target->next;
	delete target;
	return true;
}

void list::clear() {
	listNode* doomed = this->root;
	while(this->root) {
		this->root = this->root->next;
		delete doomed;
		doomed = this->root;
	}
}
void list::print() {
	/* TODO: write this. */
}

void list::reverse() {
	/* NOTE: we should do this WITHOUT allocating any memory.
	 * Just rearrange the arrows until the list is reversed.
	 * Put another way, memory footprint should be O(1). */
	/* That is, don't do this: */
	#if 0
	list R;
	for(listNode* p = this->root; p!=0; p = p->next) {
		R.insertAtFront(p->data);
	}
	*this = R;
	#endif
	/* TODO: write a proper version of this which requires only
	 * O(1) additional space. 
*/

if (this->root == 0) return;

listNode * q = 0; //node before p;
listNode * p = root; //node we're currenly reversing;
listNode * r = p->next; //node after p
//now the next node is pointing to root

while(r)



}
